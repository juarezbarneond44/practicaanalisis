# imports
import json
import sys
import time
import os
import random
import string
import time
# import datetime
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from random import randrange
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys

SELENIUM_HUB = 'http://34.125.41.136:4445/wd/hub'
driver = webdriver


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def test1():

    try:
        print("starting prueba Login")
        driver = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX,
        )

        print("opening the page")
        driver.get('http://localhost:4200/')
        time.sleep(4)
        print("se abrio la pagina")
        search_u = driver.find_element_by_id("username")
        search_u.clear()
        search_u.send_keys("admin")
        time.sleep(1)
        print("se escribio user")

        search_e = driver.find_element_by_id("password")
        search_e.clear()
        search_e.send_keys("123")
        time.sleep(1)
        print("se escribio contrasena")
        search_btn = driver.find_element_by_id("boton")
        search_btn.click()
        time.sleep(4)
        print("se logeo el usuario")

        print("prueba de login exitosa")
        time.sleep(10)
        print("prueba de Crear libro")
        search_u = driver.find_element_by_id("username")
        search_u.clear()
        search_u.send_keys("prueba")
        time.sleep(1)
        print("se escribio nombre")
        search_u = driver.find_element_by_id("precio")
        search_u.clear()
        search_u.send_keys(100)
        time.sleep(1)
        print("se escribio precio")

        search_u = driver.find_element_by_id("fecha")
        search_u.clear()
        search_u.send_keys("2020-10-10")
        time.sleep(1)
        print("se escribio fecha")
        search_u = driver.find_element_by_id("escritor")
        search_u.clear()
        search_u.send_keys("usuario")
        time.sleep(1)
        print("se escribio usuario")
        time.sleep(4)
        search_btn = driver.find_element_by_id("showBook")
        search_btn.click()
        print("existe libro")
        time.sleep(10)
        print("prueba de Crear Libro exitosa")
        driver.quit()
        return(1)
    except:
        driver.quit()
        print("error en prueba de registro")
        return (0)


def test2():
    try:
        print("starting prueba Login Fail")
        driver = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX
        )

        driver.get('http://analisis2.tk/Login')
        print("se abrio pagina login")
        time.sleep(6)

        search_login = driver.find_element_by_id("login")
        search_login.clear()
        search_login.send_keys("inexistente@gmail.com")
        time.sleep(1)
        print("se ingreso correo en el login")

        search_pas = driver.find_element_by_id("password")
        search_pas.clear()
        search_pas.send_keys("1234")
        time.sleep(1)
        print("se ingreso password en el login")

        search_btnLogin = driver.find_element_by_id("ingresar")
        search_btnLogin.click()
        time.sleep(1)
        print("este usuario de prueba no debe acceder")
        print("prueba de usuari no existente satisfactoriamente")
        time.sleep(4)
        driver.quit()
        return (1)
    except Exception as e:
        driver.quit()
        print("error en prueba de usuario no existente")
        return (0)


def __main__():
    msgError = "Error en las pruebas funcionales."
    if(test1() == 0):
        raise Exception(msgError)
    print("Pruebas Funcionales Exitosas")


__main__()
