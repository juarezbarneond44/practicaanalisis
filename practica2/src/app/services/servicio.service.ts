import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { environment } from '../environments/environments';
@Injectable({
  providedIn: 'root'
})
export class ServicioService {
  

  constructor(private http: HttpClient) { }
  public url: string = environment.url
  obtenerDatosLibro() {
    const url = this.url  
    return this.http.get(url);
  }
  CreateBook(nombre:string,precio:number,escritor:string,fecha:string) {
  
    const url = this.url + "createBook"
    return this.http.post(url,{
      "nombre":nombre,
      "precio":precio,
      "escritor":escritor,
      "fecha":fecha
    });
  }
  deleteBook(id:number) {
    const url = this.url+"deleteBook"  
    return this.http.post(url,{"id":id});
  }
  IDBook(id:number) {
    const url = this.url+"getBook"  
    return this.http.post(url,{"id":id});
  }
  EditBook(id:number,nombre:string,precio:number,escritor:string,fecha:string) {
    const url = this.url+"EditBook"  
    return this.http.post(url,{"id":id,
      "nombre":nombre,
      "precio":precio,
      "escritor":escritor,
      "fecha":fecha
    });
  }
}
 