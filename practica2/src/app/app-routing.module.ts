import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUserComponent } from './create-user/create-user.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { LoginComponent } from './login/login.component';
import { ShowBookComponent } from './show-book/show-book.component';
import { VentasComponent } from './ventas/ventas.component';

const routes: Routes = [
  { path: 'showBook', component: ShowBookComponent},
  { path: 'ventas', component: VentasComponent},
  { path: '', component: LoginComponent},
  { path: 'CreateBook', component: CreateUserComponent},
  { path: 'EditBook/:id', component: EditBookComponent},

   
]; 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
