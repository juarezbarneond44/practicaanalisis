import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { libro } from '../interfaz/libro';
import { ServicioService } from '../services/servicio.service';

@Component({
  selector: 'app-show-book',
  templateUrl: './show-book.component.html',
  styleUrls: ['./show-book.component.css']
})
export class ShowBookComponent implements OnInit {
public libros: any;
  constructor( ) {
    this.datos();
  }
 datos(){
   if(localStorage.getItem("creado")==="true")
   {
    let aux :libro={"id": 1,"nombre":"prueba","escritor":"usuario","precio":100,"fecha":"2020-10-10"};
    //this.libros=aux;
    this.libros=[aux];
    console.log(aux)
   }

 }
  ngOnInit(): void {
  }
  Editar(id:any){
  }
  Eliminar(id:any){
 
  }

}
