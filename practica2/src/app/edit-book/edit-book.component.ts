import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { libro } from '../interfaz/libro';
import { ServicioService } from '../services/servicio.service';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {

  constructor(private service:ServicioService,private rutaActiva: ActivatedRoute,private route:Router) { 
    this.id=this.rutaActiva.snapshot.params.id;
   
  }
  private id=0;
  public nombre="";
  public precio=0;
  public fecha='';
  public escritor="";
  ngOnInit(): void {
    this.service.IDBook(this.id).subscribe((res)=>{
   
      let aux=res as libro;
      this.nombre=aux.nombre+"";
      this.precio=Number(aux.precio)+0;
      this.fecha=aux.fecha+'';
      this.escritor=aux.escritor+'';
    })
  }
  Editar(){
    if(this.precio==0||this.nombre==""||this.escritor===""||this.fecha===''){
      alert("Llene todos los datos!")
      return;
    }
  if(!confirm("Desea confirmar los datos??")){return}
  this.service.EditBook(this.id,this.nombre,this.precio,this.escritor,this.fecha).subscribe((res)=>{
    if(res){
      alert("Libro modificado con exito");
        this.route.navigate(["showBook"]);
    }
  })

  }
}

