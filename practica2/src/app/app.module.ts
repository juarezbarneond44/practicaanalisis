import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ShowBookComponent } from './show-book/show-book.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { VentasComponent } from './ventas/ventas.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ShowBookComponent,
    NavBarComponent,
    VentasComponent,
    CreateUserComponent,
    EditBookComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
