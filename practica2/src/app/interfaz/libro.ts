export interface libro {
    id?:number;
    nombre?:string;
    fecha?:string;
    escritor?:string;
    precio?:number;
}