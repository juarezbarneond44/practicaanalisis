import { Component, OnInit } from '@angular/core';
import { ServicioService } from '../services/servicio.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  constructor(private service:ServicioService) { }
 public nombre="";
 public precio=0;
 public fecha="";
 public escritor="";

  ngOnInit(): void {
    localStorage.setItem("creado","false")
  }
crear(){
  if(this.precio==0||this.nombre==""||this.escritor===""||this.fecha===""){
    alert("Llene todos los datos!")
    return;
  }
  localStorage.setItem("creado","true")
  this.nombre="";
  this.precio=0;
  this.fecha="";
  this.escritor="";

  
}
}
